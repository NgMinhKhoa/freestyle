package com.example.Freestyle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreestyleApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreestyleApplication.class, args);
	}

}
